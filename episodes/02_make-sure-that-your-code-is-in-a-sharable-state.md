<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Katrin Leinweber

SPDX-License-Identifier: CC-BY-4.0
-->

# Step 2: Make sure that your code is in a sharable state

## Why?

- Otherwise others are not able to use your code.
- You might share things that you do not want to share.

## General Tips

- Make sure that your code can be run outside your organization:
  - Do not rely on internal resources such as compute or storage resources
  - Do not hard-code absolute paths
  - Use explicit imports at the top of your module
  - Clarify which dependencies are required to use your code
- Organize files in a suitable directory structure
- Structure your code in suitable "building blocks" such as functions
- Do not share internals and secrets with your code such as usernames, passwords, SSH private keys, internal IP addresses, etc.
- Find community of practice in your programming language, domain, etc.
- Check your code using suitable code analyzers

## Tips for Understandable Code

- Consistently apply a code style that is used in your community of practice:
  - Consistency is more important than the "right" code style
  - Avoid unproductive discussions around style and taste
  - Use tools linters and formatters to check and ensure compliance
  - Integrated development environments provide shortcuts to automatically format your code
- Use a consistent and light code layout
- Use specific names for scripts, variables, functions etc. which convey relevant information
- Do not "over-comment" but comment important aspects such as the big picture and "clever" tricks
- Read code of others
- Experiment with light-weight code reviews in your research group

## Example Project

- [`astronaut-analysis/compare/1-put-into-git...2-clean-up-code`](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/1-put-into-git...2-clean-up-code)
  shows the result of this step:
  - Improved directory structure
  - Made code style PEP8 compliant using the code formatter `black` and checked for further issues using `flake8`
  - Moved plotting code into separate functions
  - Clarified names
  - Added more relevant comments

## Key Points

- Make sure that others can (re-)use your code
- Do not share internals and secrets with your code
- Strive for understandable code

## Challenges

- [ ] Please find out about structuring recommendations, style guides and tools for your domain.
- [ ] Please identify improvements for your code, ask for feedback, and start implementing them.
- [ ] Do you find further improvements in the example code?
  If you do, please send us a merge request against branch [2-clean-up-code](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/tree/2-clean-up-code).

## Further Readings

- Directory structure:
  - Python: ["Application Layout" reference](https://realpython.com/python-application-layouts/), ["Hitchhiker’s Guide" to "Structuring Your Project"](https://docs.python-guide.org/writing/structure/#structure-of-the-repository), [Poetry](https://python-poetry.org/)
  - R: [Structuring R Projects](https://chrisvoncsefalvay.com/2018/08/09/structuring-r-projects/)
  - [Creating reusable Dryad data packages](https://datadryad.org/stash/best_practices)
  - [Cookiecutter: Start projects in various programming technologies from templates or create your own](https://github.com/cookiecutter/cookiecutter#categories-of-cookiecutters)
- Style guides and tools: 
  - [Google Styles Guides](https://github.com/google/styleguide)
  - Python: [PEP8](https://www.python.org/dev/peps/pep-0008/), [black](https://github.com/ambv/black), [flake8](https://flake8.pycqa.org/), [pylint](https://www.pylint.org/)
  - R: [tidyverse style guide](https://style.tidyverse.org/)
  - Awesome Collections: [linters](https://github.com/caramelomartins/awesome-linters), [code-formatters](https://github.com/rishirdua/awesome-code-formatters)
- [Git leaks](https://github.com/zricethezav/gitleaks) checks a Git repository for contained secrets.
- [pre-commit](https://pre-commit.com/) simplifies usage of local Git pre-commits such as checking for committed secrets.
  [Here](https://pre-commit.com/hooks.html) you can find an overview about tools that provide a pre-commit integration.
- [BFG Repo-Cleaner](https://rtyley.github.io/bfg-repo-cleaner/) helps to remove already commited secrets or large files.
- [Awesome list of code review resources ](https://github.com/joho/awesome-code-review)
- [Dustin Boswell, Trevor Foucher: "The Art of Readable Code", O'Reilly Media, 2011](http://shop.oreilly.com/product/9780596802301.do)
